dev: clean install dev 
	npm run dev

clean:
	rimraf dist
	rimraf node_modules

install:
	npm install

build: clean
	tsc

serve:
	node dist/index.js
