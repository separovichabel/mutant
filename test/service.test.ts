import { Address, User, BriefUser } from '../src/entities';
import { hasWord, getFilteredUser, briefUser } from '../src/services';

test('hasWord deve encontrar a palavra corretamente', () => {
    const addressApt = new Address({ suite: 'Apt. 556'});
    const addressSuiUpper = new Address({suite: 'Suite. 556'});
    const addressSuiLower = new Address({ suite: 'suite. Anything'});

    expect(hasWord(addressApt, 'suite')).toBe(false);
    expect(hasWord(addressSuiUpper, 'suite')).toBe(true);
    expect(hasWord(addressSuiUpper, 'SUITE')).toBe(true);
    expect(hasWord(addressSuiLower, 'suite')).toBe(true);
    expect(hasWord(addressSuiLower, 'SUITE')).toBe(true);
});

test('getFilteredUser deve filtrar com query string', () => {
    const users = [{name: 'x', address: {suite: 'suite'}} as User, {address: {suite: 'o'}} as User];

    expect(getFilteredUser(users, {anything: '123'})).toBe(users);
    expect(getFilteredUser(users, {filter: 'suite'})).toEqual([{name: 'x', address: {suite: 'suite'}}]);
    expect(getFilteredUser(users, {})).toBe(users);
});

test('briefUser ordenando e com usuario alterado', () => {
    const users = [
        {
            id: 1,
            name: 'Be',
            username: 'Bret',
            email: '1@3.4',
            address: {
              zipcode: '92998-3874',
              geo: {
                lng: '81.1496',
              },
            },
            phone: '1-770-736-8031 x56442',
            website: 'hildegard.org',
            company: {
              name: 'y',
            },
          },
          {
            id: 2,
            name: 'Ae',
            username: 'Antonette',
            email: 'e@m.ls',
            address: {
              street: 'Victor Plains',
            },
            phone: '010-692-6593 x09125',
            website: 'anastasia.net',
            company: {
              name: 'x',
            },
          },
    ];
    const briefUsers = [
        {name: 'Ae', email: 'e@m.ls', company: 'x'},
        {name: 'Be', email: '1@3.4', company: 'y'},
    ];
    const UnorderedBriefUsers = [
        {name: 'Be', email: '1@3.4', company: 'y'},
        {name: 'Ae', email: 'e@m.ls', company: 'x'},
    ];
    expect(briefUser(users as User[])).toEqual(briefUsers);
    expect(briefUser(users as User[])).not.toEqual(UnorderedBriefUsers);
    expect(briefUser(users as User[])).not.toEqual(users);
});
