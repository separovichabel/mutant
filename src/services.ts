import { User, Address, BriefUser } from './entities';
import axios from 'axios';
import { expression } from '@babel/template';

const SERVER_URL = 'https://jsonplaceholder.typicode.com/users';

export function getSite(users: User[]) {
    return users.map(user => user.website);
}

export async function getUsers(): Promise<User[]> {
    const resp = await axios.get(SERVER_URL);
    return Object.values(resp.data);
}

export function getFilteredUser(users: User[], query?: any): User[] {
    if (query && query.filter) {
        return users.filter((user: User) => hasWord(user.address, query.filter));
    }
    return users;
}

export function briefUser(users: User[]): BriefUser[] {
    throw new Error('Teste de bagaça');
    return users
        .sort(orderUserByName)
        .map(briefConverter);
}

export function hasWord(address: Address, word: string): boolean {
    return Object.values(address)
        .filter(fields => typeof fields === 'string')
        .map((values: string ) => values.toLocaleLowerCase())
        .some((fields: string) => fields.includes(word.toLocaleLowerCase()));
}

export function orderUserByName(u1: User | BriefUser, u2: User | BriefUser) {
    return u1.name < u2.name && -1 || u1.name > u2.name && 1 || 0;
}

export function briefConverter(user: User) {
    return new BriefUser(user);
}
