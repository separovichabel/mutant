import * as express from 'express';
import { config } from 'dotenv';
import { getUsers, getFilteredUser, briefUser, getSite } from './services';
import * as moment from 'moment';

// Configuring .env file
config();

const app = express();

// Middleware
app.use((req, res, next) => {
    console.log('Method:', req.method , ' | Path:', req.path, ' | Query:', req.query, ' | Time:', moment().format());
    next();
});

// Error Middleware
app.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
    console.error('Method:', req.method , ' | Path:', req.path, ' | Query:', req.query, ' | Time:', moment().format());
    console.error(err.stack);
    res.status(500).send('Internal Server Error');
});

// Routes
app.get('/', async (req, res) => res.send(getFilteredUser(await getUsers(), req.query)));
app.get('/website', async (req, res) => res.send(getSite(await getUsers())));
app.get('/brief', async (req, res) => res.send(briefUser(await getUsers())));

console.log(`Server up on http://localhost:${process.env.PORT}`);

// Running server
app.listen(process.env.PORT);
