export class User {
    id: number;
    name: string;
    username: string;
    email: string;
    address: Address;
    phone: string;
    website: string;
    company: Company;

    constructor(plain: any) {
        this.id = plain.id;
        this.name = plain.name;
        this.username = plain.username;
        this.email = plain.email;
        this.phone = plain.phone;
        this.website = plain.website;
        this.address = new Address(plain.address);
        this.company = new Company(plain.company);
    }
}

export class Address {
    street: string;
    suite: string;
    city: string;
    zipcode: string;
    geo: Geolocation;

    constructor(plain: any) {
        this.street = plain.street;
        this.suite = plain.suite;
        this.city = plain.city;
        this.zipcode = plain.zipcode;
        this.geo = plain.geo;
    }
}

export class Geolocation {
    lat: number;
    lng: number;

    constructor(plain: any) {
        this.lat = plain.lat;
        this.lng = plain.lng;
    }
}

export class Company {
    name: string;
    catchPhrase: string;
    bs: string;

    constructor(plain: any) {
        this.name = plain.name;
        this.catchPhrase = plain.catchPhrase;
        this.bs = plain.bs;
    }
}

export class BriefUser {
    name: string;
    email: string;
    company: string;

    constructor(user: User) {
        this.name = user.name;
        this.email = user.email;
        this.company = user.company.name;
    }
}
